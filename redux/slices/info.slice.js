import { createSlice } from '@reduxjs/toolkit';

const INITIAL_STATE = {
    personalInfo: {
        name: 'Tiago',
        surnames: 'Azevedo Pratas',
        job: 'Developer of Stuff',
        about: `Hi! I am Tiago & I am a web developer`,
        about2:['React', 'Nodejs', 'Nextjs', 'NoSQL', 'Javascript', 'Typescript', 'Death Metal'],
        links: {
            email: 'prattiago@gmail.com',
            linkedin: 'https://www.linkedin.com/in/Tiago-Pratas',
            twitter: 'Twitter/Tiago-Pratas',
            dev: 'Dev.to/TiagoPratas',
            git: 'www.github.com',
        },
    },
    projects: [
        {
        title: 'Kaban Board',
        img: 'https://github.com/Tiago-Pratas/Kabanboard/raw/master/src/assets/KabanBoard.png',
        link: 'https://kabanboard-hgqtvhyly-tiago-pratas.vercel.app',
        description: 'Kaban Board made using Material CDK.',
        technologies: ['Angular', 'Material UI', 'Typescript']
        },
        {
            title: 'Streamly',
            img: 'https://i.ibb.co/7b4vV3d/Screenshot-2021-06-23-at-08-24-21.png',
            link: 'https://kabanboard-hgqtvhyly-tiago-pratas.vercel.app',
            description: 'Service that allows users to track their favourite OTTs',
            technologies: ['React', 'NodeJS', 'MongoDB', 'Nodemailer', 'Zeplin']
        },
        {
            title: 'The comic Books',
            img: 'https://i.ibb.co/N6vQhFF/Screenshot-2021-06-21-at-09-12-54.png',
            link: 'https://kabanboard-hgqtvhyly-tiago-pratas.vercel.app',
            description: 'Service that allows users to track their comics collection',
            technologies: ['React', 'NodeJS', 'MongoDB', 'Nodemailer', 'Zeplin', 'Handlebars', 'Passport']
        },
        {
            title: 'Portfolio',
            img: 'https://i.ibb.co/D9W8psX/Screenshot-2021-06-23-at-08-27-20.png',
            link: 'https://kabanboard-hgqtvhyly-tiago-pratas.vercel.app',
            description: 'My own portfolio which you are looking at right now.',
            technologies: ['React', 'NodeJS', 'NextJS']
            },
    ],


};

const infoSlice = createSlice({
    name: 'info',
    initialState: INITIAL_STATE,
});

export default infoSlice;