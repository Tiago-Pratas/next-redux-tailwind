import { createSlice } from '@reduxjs/toolkit';

const INITIAL_STATE = {
    name: "",
    email: "",
    message: "",
    isSubmitted: true,
}