import * as Components from '../components/index';


export default function Home() {
  return (
      <div className="flex flex-col">
        <Components.About />
        <div className="bg-gray-100 h-60 transform-gpu skew-y-6 rotate-11 translate-y-40 z-0">
        </div>
        <div>
        <Components.Work className="bg-gray-500" />
        </div>
      </div>
  )
}
