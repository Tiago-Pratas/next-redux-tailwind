import dotenv from 'dotenv';
import nodemailer from 'nodemailer';

dotenv.config();

export default async function contact(req, res) {

  const transporter = nodemailer.createTransport({
    service: process.env.EMAIL_SERVICE,
    auth: {
        user: process.env.EMAIL_ACC,
        pass: process.env.EMAIL_PWD,
    },
  });

  await transporter.sendMail({
    from: '"the Comic Books" <no-reply@the-comic-books.com>',
    to: process.env.EMAIL_ACC,
    subject: `email contact from ${req.body.name}`,
    text: `Email from ${req.body.email}`,
    html: `<p>${req.body.message}</p>`,
});
  return res.status(200)
}
