import Head from 'next/head';
import { Provider } from 'react-redux';
import store from '../redux/store';
import * as Components from '../components/index';
import '../styles/globals.css'

function App({ Component, pageProps }) {

  return (<>
      <Head>
        <title>Tiago Pratas' Portfolio</title>
        <link rel="icon" href="/portfolio_logo.png"/>
        <link rel="preconnect" href="https://fonts.googleapis.com"/>
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
        <link href="https://fonts.googleapis.com/css2?family=Bungee+Shade&display=swap" rel="stylesheet" />
      </Head>
    <Provider store={store}>
      <div className="bg-gray-100 h-full">
        <Components.Header/>
        <div className="flex justify-center py-10">
          <Component {...pageProps} />
        </div>
      </div>
    </Provider>
    </>
  )
}

export default App;
