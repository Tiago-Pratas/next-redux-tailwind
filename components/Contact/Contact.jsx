import { useState } from 'react';

const Contact = () => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');
    const [submitted, setSubmitted] = useState(false);

    const handleSubmit = (e) => { 
        e.preventDefault()

        console.log('Sending');

        let data = {
            name,
            email,
            message
            };

        fetch('/api/contact', {
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
            })
            .then((res) => {
            console.log('Response received')
            if (res.status === 200) {
                console.log('Response succeeded!')
                setSubmitted(true)
                setName('')
                setEmail('')
                setMessage('')
                }
            }
        )
    }
    return(
    <div className="bg-waves m-0 bg-fit bg-center bg-no-repeat bg-center bg-clip-border bg-fixed">
        <div className="max-w-2xl bg-transparent py-10 px-5 m-auto w-full mt-10">

        <div className="text-3xl mb-6 text-center ">
            Get in Touch
        </div>

                </div>

        <div className="grid grid-cols-2 gap-4 max-w-xl m-auto">

            <div className="col-span-2 lg:col-span-1">
                <input type="text" className="opacity-80 border-solid border-gray-400 rounded border-2 p-3 md:text-xl w-full"
                onChange={(name)=>{setName(name.target.value)}} 
                placeholder="Name" name="name"/>
            </div>

            <div className="col-span-2 lg:col-span-1">
                <input type="text" className="opacity-80 border-solid border-gray-400 rounded border-2 p-3 md:text-xl w-full"
                onChange={(email)=>{setEmail(email.target.value)}} 
                placeholder="Email Address" name="email"/>
            </div>

            <div className="col-span-2">
                <textarea cols="30" rows="8" className="opacity-80 border-solid rounded border-gray-400 border-2 p-3 md:text-xl w-full" 
                placeholder="Message"
                onChange={(message)=>{setMessage(message.target.value)}} 
                name="message"></textarea>
            </div>

            <div className="col-span-2 text-right">
                <button className="py-3 px-6 bg-customBlue text-gray-400 font-bold w-full sm:w-32 rounded-xl "
                    onClick={(e)=>{handleSubmit(e)}}>
                        Send
                </button>
            </div>
            </div>
    </div>

    )
};

export default Contact;