import React from 'react';
import Image from 'next/image'

const Navbar = () => {
    return <header class="text-gray-600 body-font">
    <div class="flex flex-wrap p-5 flex-col md:flex-row items-center bg-gray-400 rounded">
      <a class="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0 border-r-2 border-gray-300 px-3">
        <Image src="/portfolio_logo.png" width={60} height={60} class="w-10 h-10p-2 bg-gray-300 rounded-full"/>
      </a>
      <nav class="md:ml-auto md:mr-auto flex flex-wrap items-center text-base justify-center">
        <a class="mr-5 text-gray-300 text-xl hover:text-blue-300 cursor-pointer">About me</a>
        <a class="mr-5 text-gray-300 text-xl hover:text-blue-300 cursor-pointer">Work</a>
        <a class="mr-5  text-gray-300 text-xl hover:text-blue-300 cursor-pointer">Contact</a>
      </nav>
    </div>
  </header>
};

export default Navbar;