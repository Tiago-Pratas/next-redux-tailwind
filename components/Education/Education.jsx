import { useSelector } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircle } from '@fortawesome/free-solid-svg-icons';

const Education = (props) => {
    window.scroll({
        top: 10000,
    });

    
    const { education } = useSelector(state => state.info);
    console.log(education)

    return(<section className="text-gray-600 body-font">
                <div className="container px-5 py-2 mx-auto">
                    <div className="flex flex-wrap w-full mb-10">
                    <div className="lg:w-1/2 w-full mb-6 lg:mb-0">
                        <h1 className="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-600">Education</h1>
                        <div className="h-1 w-70 bg-yellow-400 rounded"></div>
                    </div>
                    </div>
                    </div>
                    {
                        education.map(education => (<>
                            <div className="h-1 w-10 bg-yellow-400 rounded"></div>
                            <div className="flex-grow pl-6 text-left">
                        <h2 className="tracking-widest text-xs title-font font-medium text-indigo-500 mb-1"></h2>
                        <h1 className="title-font text-xl font-medium text-gray-600 mb-">{education.institution}</h1>
                        <p className="leading-relaxed mb-5">{education.qualification}</p>
                        <a className="inline-flex items-center">
                        <img alt='institution icon' src={education.img} className="w-8 h-8 rounded-full flex-shrink-0 object-cover object-center"/>
                        <span className="flex-grow flex flex-col pl-3">
                            <span className="title-font font-medium text-gray-900">{education.completion}</span>
                        </span>
                        </a>
                    </div>
                    </>
                        ))
                    }
        </section>);

}

export default Education;