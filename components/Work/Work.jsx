import React from 'react';
import { useSelector } from 'react-redux'


const projects = () => {
    const { projects } = useSelector(state => state.info);

    console.log(projects[0].img);

    return <section className="text-gray-600 body-font bg-gray-300 py-80 px-20">
    <div className="container px-5 py-2 mx-auto">
        <div className="flex flex-wrap w-full mb-10">
        <div className="lg:w-1/2 w-full mb-6 lg:mb-0">
            <h1 className="sm:text-3xl text-2xl font-metal-mania font-medium title-font mb-2 bg-clip-text bg-gradient-to-tl from-blue-300 via-black to-red-500">Stuff I have made myself</h1>
            <div className="h-1 w-70 bg-yellow-400 rounded bg-gradient-to-r from-blue-300 via-black to-red-500"></div>
        </div>
        </div>
        </div>
        {
            projects.map(project => (<>
            <div className="flex py-4"></div>
                <div className="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4">
                <div className="p-4 md:w-1/3 sm:mb-0 mb-6">
                    <div className="rounded-lg h-64 overflow-hidden">
                        <img alt="content" className="object-cover object-center h-full w-full" src={project.img}/>
                    </div>
                    <h2 className="text-xl font-medium title-font text-gray-900 mt-5">{project.title}</h2>
                    <p className="text-base leading-relaxed mt-2">{project.description}</p>
                    <a className="text-indigo-500 inline-flex items-center mt-3" href={project.link}>Learn More
                        <svg fill="none" 
                            stroke="currentColor" 
                            stroke-linecap="round" 
                            stroke-linejoin="round" 
                            stroke-width="2" 
                            className="w-4 h-4 ml-2" viewBox="0 0 24 24">
                            <path d="M5 12h14M12 5l7 7-7 7"></path>
                        </svg>
                    </a>
                </div>
            </div>
      </>
            ))
        }
    
    </section>;
};

export default projects;