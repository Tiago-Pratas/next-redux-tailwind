import Header from './Header/Header.jsx';
import Contact from './Contact/Contact.jsx'
import About from './About/About.jsx';
import Work from './Work/Work.jsx';

export {
    Header,
    Contact,
    About,
    Work,
}